﻿using System;

namespace MyCity.WebHost.Models
{
    public class RoleResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
