﻿using System;

namespace MyCity.WebHost.Models
{
    public class ReviewResponse
    {
        public Guid Id { get; set; }
        public required string Text { get; set; }
        public int Rating { get; set; }
    }
}
