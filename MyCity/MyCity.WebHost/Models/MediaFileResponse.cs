﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MyCity.Dtos.Enums;

namespace MyCity.WebHost.Models
{
    public class MediaFileResponse
    {
        public Guid Id { get; set; }
        public required Uri Uri { get; set; }
        public string? Description { get; set; }
        public MediaFileType MediaFileType { get; set; }
    }
}
