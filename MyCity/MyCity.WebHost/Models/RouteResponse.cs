﻿using MyCity.Core.Domain;
using System;
using System.Collections.Generic;

namespace MyCity.WebHost.Models
{
    public class RouteResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<CustomStop> CustomStops { get; set; }
        public List<Review> Reviews { get; set; }
    }
}
