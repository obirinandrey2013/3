﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using MyCity.Core.Abstractions.Services;
using MyCity.Services;
using MyCity.WebHost.Mapper;
using System.Reflection;
using OpenTelemetry.Logs;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using Microsoft.Extensions.Logging;
using Npgsql;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;

namespace MyCity.WebHost.Extensions
{
    public static class StartupExtensions
    {
        public static IServiceCollection ConfigureMapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile<MapperProfile>(); });

            var mapper = mappingConfig.CreateMapper();
            return services.AddSingleton(mapper);
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services.AddScoped<IRoleService, RoleService>()
                    .AddScoped<IUserService, UserService>()
                    .AddScoped<IMediaFileService, MediaFileService>()
                    .AddScoped<IReviewService, ReviewService>()
                    .AddScoped<ICustomStopService, CustomStopService>()
                    .AddScoped<IPointOfInterestService, PointOfInterestService>()
                    .AddScoped<IRouteService, RouteService>()
                ;
        }

        public static IServiceCollection AddTelemetry(this IServiceCollection builder)
        {
            var serviceName = Assembly.GetExecutingAssembly().GetName().Name ?? "MyCity";
            var serviceVersion = Assembly.GetExecutingAssembly().GetName().Version?.ToString();

            builder.AddOpenTelemetry()
                .ConfigureResource(resource => resource.AddService(
                    serviceName: serviceName,
                    serviceVersion: serviceVersion))
                .WithTracing(tracing => tracing
                    .AddSource(serviceName)
                    .AddAspNetCoreInstrumentation()
                    .AddNpgsql()
                    .AddHttpClientInstrumentation()
                    .AddOtlpExporter()
                );

            builder.AddLogging(logging =>
                logging.AddOpenTelemetry(options =>
                    options.SetResourceBuilder(ResourceBuilder.CreateDefault()
                            .AddService(
                                serviceName: serviceName,
                                serviceVersion: serviceVersion))
                        .AddOtlpExporter()
                        .AddConsoleExporter()
                )
            );

            return builder;
        }

        public static IServiceCollection AddAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(cfg =>
            {
                cfg.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                cfg.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                cfg.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = false;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8
                        .GetBytes(configuration["ApplicationSettings:JWT_Secret"])
                    ),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });
            return services;
        }
    }
}
