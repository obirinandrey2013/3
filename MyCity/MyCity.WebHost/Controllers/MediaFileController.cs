﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyCity.Core.Abstractions.Services;
using MyCity.Dtos.MediaFileDtos;
using MyCity.WebHost.Models;

namespace MyCity.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MediaFileController : ControllerBase
    {
        private readonly IMediaFileService _mediaFileService;
        private readonly IMapper _mapper;
        private readonly ILogger<MediaFileController> _logger;

        public MediaFileController(IMediaFileService mediaFileService, IMapper mapper, ILogger<MediaFileController> logger)
        {
            _mediaFileService = mediaFileService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<List<MediaFileResponse>>> GetAllAsync()
        {
            return Ok(_mapper.Map<List<MediaFileResponse>>(await _mediaFileService.GetAllAsync()));
        }

        [HttpGet("{id:Guid}")]
        public async Task<ActionResult<MediaFileResponse>> GetAsync(Guid id)
        {
            var result = await _mediaFileService.GetAsync(id);
            if (result != null)
                return Ok(_mapper.Map<MediaFileResponse>(result));

            _logger.LogWarning("Not found object with {@Id}", id);
            return NotFound();
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> CreateAsync(CreateOrUpdateMediaFileDto mediaFile)
        {
            var result = await _mediaFileService.CreateAsync(_mapper.Map<CreateOrUpdateMediaFileDto>(mediaFile));
            return Ok(result);
        }

        [HttpPut("{id:Guid}")]
        public async Task<ActionResult> UpdateAsync(Guid id, CreateOrUpdateMediaFileDto mediaFile)
        {
            var result = await _mediaFileService.UpdateAsync(id, _mapper.Map<CreateOrUpdateMediaFileDto>(mediaFile));
            if (result)
                return Ok();

            _logger.LogWarning("Not found object with {@Id}", id);
            return NotFound();
        }

        [HttpDelete("{id:Guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            var result = await _mediaFileService.DeleteAsync(id);
            if (result)
                return Ok();

            _logger.LogWarning("Not found object with {@Id}", id);
            return NotFound();
        }
    }
}
