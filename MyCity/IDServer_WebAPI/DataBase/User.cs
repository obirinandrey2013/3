﻿using Microsoft.AspNetCore.Identity;

namespace IDServer_WebAPI.DataBase
{
    public class User:IdentityUser
    {
        public string? Initials {  get; set; }
    }
}
