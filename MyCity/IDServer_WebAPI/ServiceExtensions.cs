﻿using System.Reflection;
using IDServer_WebAPI.DataBase;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using OpenTelemetry.Logs;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;

namespace IDServer_WebAPI;

public static class ServiceExtensions
{
    public static void ApplyMigrations(this IApplicationBuilder app)
    {
        using IServiceScope scope = app.ApplicationServices.CreateScope();
        using ApplicationDBContext context = scope.ServiceProvider.GetRequiredService<ApplicationDBContext>();
        context.Database.Migrate();
    }


    public static IServiceCollection AddTelemetry(this IServiceCollection builder)
    {
        var serviceName = Assembly.GetExecutingAssembly().GetName().Name ?? "IDServer";
        var serviceVersion = Assembly.GetExecutingAssembly().GetName().Version?.ToString();

        builder.AddOpenTelemetry()
            .ConfigureResource(resource => resource.AddService(
                serviceName: serviceName,
                serviceVersion: serviceVersion))
            .WithTracing(tracing => tracing
                .AddSource(serviceName)
                .AddAspNetCoreInstrumentation()
                .AddNpgsql()
                .AddHttpClientInstrumentation()
                .AddOtlpExporter()
            );

        builder.AddLogging(logging =>
            logging.AddOpenTelemetry(options =>
                options.SetResourceBuilder(ResourceBuilder.CreateDefault()
                        .AddService(
                            serviceName: serviceName,
                            serviceVersion: serviceVersion))
                    .AddOtlpExporter()
                    .AddConsoleExporter()
            )
        );

        return builder;
    }
}