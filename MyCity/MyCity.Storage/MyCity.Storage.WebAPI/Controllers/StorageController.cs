using Microsoft.AspNetCore.Mvc;
using Minio;
using Minio.DataModel.Args;

namespace MyCity.Storage.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class StorageController(IMinioClientFactory minioClientFactory) : ControllerBase
{
    [HttpPost]
    public async Task<ActionResult> PutFileAsync(IFormFile file)
    {
        var minioClient = minioClientFactory.CreateClient();

        var beArgs = new BucketExistsArgs()
            .WithBucket("mycitys3");
        bool found = await minioClient.BucketExistsAsync(beArgs);
        if (!found)
        {
            var mbArgs = new MakeBucketArgs()
                .WithBucket("mycitys3");
            await minioClient.MakeBucketAsync(mbArgs);
        }

        var putObjectArgs = new PutObjectArgs()
            .WithBucket("mycitys3")
            .WithObject("111/" + file.FileName)
            .WithStreamData(file.OpenReadStream())
            .WithObjectSize(file.Length)
            .WithContentType("application/octet-stream");

        var res = await minioClient.PutObjectAsync(putObjectArgs);
            
        return Ok(res.Etag);
    }

    [HttpGet("{id:Guid}")]
    public async Task<ActionResult> GetLinkAsync(Guid id)
    {
        var minioClient = minioClientFactory.CreateClient();
        var presignedGetObjectArgs = new PresignedGetObjectArgs()
            .WithBucket("mycitys3")
            .WithObject("111/minio.png")
            .WithExpiry(60 * 60 * 24);

        var url = await minioClient.PresignedGetObjectAsync(presignedGetObjectArgs);
        return Ok(url);
    }

}