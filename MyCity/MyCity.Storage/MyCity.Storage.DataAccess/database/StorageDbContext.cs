﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyCity.Storage.Core.Domain;

namespace MyCity.Storage.DataAccess.database;

internal class StorageDbContext(DbContextOptions<StorageDbContext> options) : DbContext(options)
{
    public DbSet<StorageMediaFile> StorageMediaFiles { get; set; }
}