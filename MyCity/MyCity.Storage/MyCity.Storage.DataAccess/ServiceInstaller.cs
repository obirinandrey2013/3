using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Minio;

namespace MyCity.Storage.DataAccess;

public static class ServiceInstaller
{
    public static IServiceCollection AddStorage(this IServiceCollection services, IConfiguration configuration)
    {
        var endpoint = configuration.GetValue<string>("Storage:Endpoint");
        var accessKey = configuration.GetValue<string>("Storage:AccessKey");
        var secretKey = configuration.GetValue<string>("Storage:SecretKey");
        services.AddMinio(configureClient => configureClient
                .WithEndpoint(endpoint, 9000)
                .WithSSL(false)
                .WithCredentials(accessKey, secretKey)
                .Build()
        );
        return services;
    }
}