﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCity.Storage.Core.Domain;

public class StorageMediaFile
{
    public Guid Id { get; set; }
    public string FileName { get; set; }
    public string ObjectName { get; set; }
}