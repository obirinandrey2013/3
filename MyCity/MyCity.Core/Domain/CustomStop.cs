﻿namespace MyCity.Core.Domain
{
    public class CustomStop : BaseEntity
    {
        public Guid PointOfInterestId { get; set; }
        public string? Description { get; set; }
        public List<MediaFile>? MediaFiles { get; set; }
    }
}
